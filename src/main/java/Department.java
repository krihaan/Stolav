import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;

public class Department {
    private String departmentName;
    private ArrayList<Employee>employees;
    private ArrayList<Patient>patients;

    public Department(String departmentName) {
        this.departmentName = departmentName;
        employees=new ArrayList<>();
        patients=new ArrayList<>();
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public ArrayList<Employee> getEmployees(){
        return employees;
    }

    public void addEmployee(Employee e){
        employees.add(e);
    }

    public ArrayList<Patient> getPatients(){
        return patients;
    }

    public void addPatients(Patient p){
        patients.add(p);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName) &&
                Objects.equals(employees, that.employees) &&
                Objects.equals(patients, that.patients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName, employees, patients);
    }

    public void remove(Person person) throws RemoveException {
        boolean isEmployee=person instanceof Employee;
        boolean isPatient=person instanceof Patient;

        boolean found;

        if (isEmployee){
            found=employees.contains(person);
            employees.remove(person);
        }
        else if(isPatient){
            found=patients.contains(person);
            patients.remove(person);
        }
        else {
            found=false;
        }
        if (!found){
            throw new RemoveException("Unable to remove "+person+" from department "+departmentName);
        }
    }

    @Override
    public String toString() {
        String string=getDepartmentName()+"\n" +
                "       Employees:\n";
        for (Employee e:employees){
            string+="           "+e.toString()+"\n";
        }
        string+="       Patients:\n";
        for (Patient p:patients) {
            string+="           "+p.toString()+"\n";
        }
        return string;
    }
}
