public class Patient extends Person implements Diagnosable{
    private String diagnosis;

    public Patient(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
        diagnosis="not determined";
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString() {
        return "Patient: "+getFullName()+
                " has diagnosis: "+getDiagnosis();
    }
}
