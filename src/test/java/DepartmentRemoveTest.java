import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class DepartmentRemoveTest {
    @Test
    public void removeRemovesEmployees() {
        Department test=new Department("test");

        Employee employee=new Employee("Ansatt","Ansatt","000");
        Patient patient=new Patient("Pasient","Pasient","001");
        Surgeon surgeon=new Surgeon("Ansatt","Ansatt","000");

        test.addEmployee(employee);
        test.addPatients(patient);
        test.addEmployee(surgeon);

        //positive tests
        try {
            test.remove(employee);
            assertTrue(true);
        }
        catch (RemoveException e){
            assertTrue(false,"Could not remove employee");
        }

        try {
            test.remove(surgeon);
            assertTrue(true);
        }
        catch (RemoveException e){
            assertTrue(false,"Could not remove Surgeon");
        }


        try {
            test.remove(patient);
            assertTrue(true);
        }
        catch (RemoveException e){
            assertTrue(false,"Could not remove Patient");
        }

        test.addEmployee(employee);
        test.addPatients(patient);
        Employee employee1=new Employee("Pasient","Pasient","001");
        Patient patient1=new Patient("Ansatt","Ansatt","000");


        //Negative tests
        try {
            test.remove(employee1);
            assertTrue(false,"Removed a non existing employee");
        }
        catch (RemoveException e){
            assertTrue(true);
        }

        try {
            test.remove(patient1);
            assertTrue(false, "Removed a non existing patient");
        }
        catch (RemoveException e){
            assertTrue(true);
        }

        try {
            test.remove(null);
            assertTrue(false, "Remove null");
        }
        catch (RemoveException e){
            assertTrue(true);
        }

    }

}
