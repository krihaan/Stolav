public class HospitalClient {
    private Hospital hospital;

    public static void main(String[] args) {
        HospitalClient hospitalClient=new HospitalClient();

        hospitalClient.taskFiveClient();

    }

    public HospitalClient() {
        this.hospital=new Hospital("St. Olavs hospital");

        HospitalTestData.fillRegisterWithTestData(hospital);
    }

    public void taskFiveClient() {


        System.out.println("Welcome to the hospital");
        System.out.println("This is the status of the hospital");
        System.out.println(hospital.toString());
        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("");
        System.out.println("");
        System.out.println("");

        Department dep=hospital.getDepartments().get(0);
        Employee e=dep.getEmployees().get(0);

        System.out.println("Tries to remove "+e.toString()+" from "+dep.getDepartmentName());

        try {
            dep.remove(e);
            System.out.println("    Success!");
        }
        catch (RemoveException exception){
            System.out.println("    Failure!");
        }


        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("This is the current status of the hospital");
        System.out.println(hospital.toString());
        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("");
        System.out.println("");
        System.out.println("");

        Patient p=new Patient("Not","Real","404");

        System.out.println("Tries to remove "+p.getFullName()+" from "+dep.getDepartmentName());

        try {
            dep.remove(p);
            System.out.println("    Success!");
        }
        catch (RemoveException exception){
            System.out.println("    Failure!");
        }

    }
}
