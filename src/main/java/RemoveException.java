public class RemoveException extends Exception {
    static final long serialVersionUID=1L;

    public RemoveException(String message){
        super(message);
    }
}
