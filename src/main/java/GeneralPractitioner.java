public class GeneralPractitioner extends Doctor {
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    public void setDiagnosis(String diagnosis, Patient p){
        p.setDiagnosis(diagnosis);
    }
}
