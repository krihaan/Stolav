public class Surgeon extends Doctor{
    public Surgeon(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    public void setDiagnosis(String diagnosis, Patient p){
        p.setDiagnosis(diagnosis);
    }
}
