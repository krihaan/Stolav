public abstract class Doctor extends Employee{
    public Doctor(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    public abstract void setDiagnosis(String diagnosis, Patient p);
}
