import java.util.ArrayList;

public class Hospital {
    private final String HOSPITALNAME;
    private ArrayList<Department>departments;

    public Hospital(String HOSPITALNAME) {
        this.HOSPITALNAME = HOSPITALNAME;
        departments=new ArrayList<>();
    }

    public String getHospitalName() {
        return HOSPITALNAME;
    }

    public ArrayList<Department> getDepartments(){
        return departments;
    }

    public void addDepartment(Department d){
        departments.add(d);
    }

    @Override
    public String toString() {
        String string=HOSPITALNAME+":\n";
        for (Department d:departments){
            string+="   "+d.toString()+"\n";
        }
        return string;

    }
}
